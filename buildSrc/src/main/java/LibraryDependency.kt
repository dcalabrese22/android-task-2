object LibraryDependency {
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib:${LibraryVersion.KOTLIN}"
    const val CORE_KTX = "androidx.core:core-ktx:${LibraryVersion.CORE_KTX}"
    const val APP_COMPAT = "androidx.appcompat:appcompat:${LibraryVersion.APP_COMPAT}"
    const val MATERIAL = "com.google.android.material:material:${LibraryVersion.MATERIAL}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${LibraryVersion.RETROFIT}"
    const val RETROFIT_COROUTINES_ADAPTER = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${LibraryVersion.RETROFIT_COROUTINES_ADAPTER}"
    const val RETROFIT_GSON_CONVERTER = "com.squareup.retrofit2:converter-gson:${LibraryVersion.RETROFIT}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${LibraryVersion.COROUTINES}"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${LibraryVersion.COROUTINES}"
    const val LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${LibraryVersion.LOGGING_INTERCEPTOR}"
    const val VIEWMODEL_KTX = "androidx.lifecycle:lifecycle-viewmodel-ktx:${LibraryVersion.VIEWMODEL_KTX}"
    const val DESUGAR_JDK_LIBS = "com.android.tools:desugar_jdk_libs:${LibraryVersion.DESUGAR_JDK_LIBS}"

    const val JUNIT = "junit:junit:${LibraryVersion.JUNIT}"
    const val ARCH_CORE_TESTING = "androidx.arch.core:core-testing:${LibraryVersion.ARCH_CORE_TESTING}"
    const val COROUTINES_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${LibraryVersion.COROUTINES}"
}

object LibraryVersion {
    const val KOTLIN = "1.5.0"
    const val CORE_KTX = "1.5.0"
    const val APP_COMPAT = "1.3.0"
    const val MATERIAL = "1.3.0"
    const val RETROFIT = "2.9.0"
    const val COROUTINES = "1.5.0"
    const val RETROFIT_COROUTINES_ADAPTER = "0.9.2"
    const val LOGGING_INTERCEPTOR = "4.9.1"
    const val VIEWMODEL_KTX = "2.3.1"
    const val JUNIT = "4.13.2"
    const val ARCH_CORE_TESTING = "2.1.0"
    const val DESUGAR_JDK_LIBS = "1.1.5"
}
package com.treelineinteractive.recruitmenttask.data.network.model

import com.google.gson.annotations.SerializedName

data class ProductItem(
    @SerializedName("id") val id: String,
    @SerializedName("type") val type: String,
    @SerializedName("color") val color: String,
    @SerializedName("available") var available: Int,
    @SerializedName("cost") val cost: Float,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    var sold: Int = 0
) {

    var position = -1

    fun addSale() {
        if (canSell()) {
            sold++
            available--
        }
    }

    fun removeSale() {
        if (canRemoveSale()) {
            sold--
            available++
        }
    }

    fun canSell(): Boolean = available > 0
    fun canRemoveSale(): Boolean = sold > 0

    fun resetSales() {
        sold = 0
    }

    fun calculateRevenue(): Float {
        return sold * cost
    }
}
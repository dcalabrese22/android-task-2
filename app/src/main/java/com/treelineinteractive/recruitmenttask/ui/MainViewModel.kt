package com.treelineinteractive.recruitmenttask.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.repository.Repository
import com.treelineinteractive.recruitmenttask.data.repository.RepositoryRequestStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.lang.StringBuilder
import java.time.LocalDate

class MainViewModel(private val shopRepository: Repository) : BaseViewModel<MainViewModel.MainViewState, MainViewModel.MainViewAction>(MainViewState()) {

    data class MainViewState(
        val dataRequest: DataRequest = DataRequest(),
        val reportEmail: ReportEmail = ReportEmail()
    ) : BaseViewState {
        val isLoadingSuccess: Boolean
            get() = !dataRequest.isLoading && dataRequest.error == null
        val sendReport: Boolean
            get() = reportEmail.body != null
    }

    data class DataRequest(
        val isLoading: Boolean = false,
        val error: String? = null,
        val items: List<ProductItem> = listOf()
    )

    data class ReportEmail(
        val body: String? = null
    )

    sealed class MainViewAction : BaseAction {
        object LoadingProducts : MainViewAction(), Reducer {
            override fun reduce(state: MainViewState): MainViewState {
                return state.copy(
                    dataRequest = state.dataRequest.copy(isLoading = true, error = null),
                    reportEmail = ReportEmail()
                )
            }
        }
        data class ProductsLoaded(val items: List<ProductItem>) : MainViewAction(), Reducer {
            override fun reduce(state: MainViewState): MainViewState {
                return state.copy(
                    dataRequest = state.dataRequest.copy(isLoading = false, error = null, items = items),
                    reportEmail = ReportEmail()
                )
            }
        }
        data class ProductsLoadingError(val error: String) : MainViewAction(), Reducer {
            override fun reduce(state: MainViewState): MainViewState {
                return state.copy(
                    dataRequest = state.dataRequest.copy(isLoading = false, error = error),
                    reportEmail = ReportEmail()
                )
            }
        }
        data class SendEmailReport(val body: String) : MainViewAction(), Reducer {
            override fun reduce(state: MainViewState): MainViewState {
                return state.copy(dataRequest = state.dataRequest,
                    reportEmail = state.reportEmail.copy(body = body)
                )
            }
        }
        object EmailReportInProgress : MainViewAction(), Reducer {
            override fun reduce(state: MainViewState): MainViewState {
                return state.copy(
                    dataRequest = state.dataRequest,
                    reportEmail = ReportEmail()
                )
            }
        }
        data class EmailReportComplete(val items: List<ProductItem>) : MainViewAction(), Reducer {
            override fun reduce(state: MainViewState): MainViewState {
                return state.copy(
                    dataRequest = state.dataRequest.copy(isLoading = false, error = null, items = items),
                    reportEmail = ReportEmail()
                )
            }
        }
    }

    init {
        loadProducts()
    }

    fun loadProducts() {
        sendAction(MainViewAction.LoadingProducts)
        viewModelScope.launch {
            shopRepository.getProducts()
                .collect { result ->
                    when (result.requestStatus) {
                        is RepositoryRequestStatus.FETCHING -> {
                            sendAction(MainViewAction.LoadingProducts)
                        }
                        is RepositoryRequestStatus.COMPLETE -> {
                            sendAction(MainViewAction.ProductsLoaded(result.data))
                        }
                        is RepositoryRequestStatus.Error -> {
                            sendAction(MainViewAction.ProductsLoadingError("Oops, something went wrong"))
                        }
                    }
                }
        }
    }

    fun createSalesReport() {
        viewModelScope.launch(Dispatchers.Default) {
            val builder = StringBuilder()
            builder.append("Sales Report for ${LocalDate.now()}\n\n")
            state.dataRequest.items.forEach {
                builder.append("${it.title}\n")
                builder.append("Sold: ${it.sold}\n\n")
            }
            sendAction(MainViewAction.SendEmailReport(builder.toString()))
        }
    }

    fun updateInventory() {
        viewModelScope.launch(Dispatchers.Default) {
            val list = state.dataRequest.items.map { it.copy() }
            list.forEach {
                it.resetSales()
            }
            sendAction(MainViewAction.EmailReportComplete(list))
        }
    }

    override fun onReduceState(viewAction: MainViewAction): MainViewState = when (viewAction) {
        is Reducer -> viewAction.reduce(state)
        else -> state
    }
}

interface Reducer {
    fun reduce(state: MainViewModel.MainViewState) : MainViewModel.MainViewState
}

class MainViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(repository) as T
    }

}
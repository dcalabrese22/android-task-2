package com.treelineinteractive.recruitmenttask.ui

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.treelineinteractive.recruitmenttask.R

class SendEmailDialogFragment : DialogFragment() {

    companion object {
        fun newInstance(): SendEmailDialogFragment {
            return SendEmailDialogFragment()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val mainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]

        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.e_mail_sales_report)
            .setMessage(R.string.email_confirm_message)
            .setPositiveButton(R.string.confirm) {_,_ ->
                mainViewModel.createSalesReport()
                dismiss()
            }
            .setNegativeButton(R.string.cancel) {_, _ ->
                dismiss()
            }
        return builder.create()
    }
}
package com.treelineinteractive.recruitmenttask.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.google.android.material.card.MaterialCardView
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding
import com.treelineinteractive.recruitmenttask.ui.ItemListener

class ProductItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.materialCardViewStyle
) : MaterialCardView(context, attrs, defStyleAttr) {

    init {
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        params.topMargin = 16
        params.marginEnd = 16
        params.marginStart = 16
        layoutParams = params
    }

    private val binding  = ViewProductItemBinding.inflate(LayoutInflater.from(context), this)

    fun setProductItem(productItem: ProductItem, listener: ItemListener) {
        binding.nameLabel.text = productItem.title
        binding.descriptionLabel.text = productItem.description
        binding.available.text = resources.getString(
            R.string.concat_two_strings_with_space,
            resources.getText(R.string.available),
            productItem.available.toString()
        )
        binding.soldCount.text = resources.getString(
            R.string.concat_two_strings_with_space,
            resources.getText(R.string.sold),
            productItem.sold.toString()
        )
        binding.addSale.isEnabled = productItem.canSell()
        binding.addSale.setOnClickListener {
            productItem.addSale()
            listener.itemClicked(productItem.position)
        }
        binding.removeSale.isEnabled = productItem.canRemoveSale()
        binding.removeSale.setOnClickListener {
            productItem.removeSale()
            listener.itemClicked(productItem.position)
        }
    }
}
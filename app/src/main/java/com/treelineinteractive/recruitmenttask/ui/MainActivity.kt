package com.treelineinteractive.recruitmenttask.ui

import android.content.ActivityNotFoundException
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.treelineinteractive.recruitmenttask.R
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private lateinit var mainViewModel: MainViewModel

    private lateinit var emailLauncher: ActivityResultLauncher<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel = ViewModelProvider(this, MainViewModelFactory(ShopRepository()))[MainViewModel::class.java]

        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }

        val adapter = ProductsRecyclerAdapter()
        with(binding.itemsLayout) {
            this.adapter = adapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        mainViewModel.stateLiveData.observe { state ->
            binding.progressBar.isVisible = state.dataRequest.isLoading
            binding.errorLayout.isVisible = state.dataRequest.error != null
            binding.errorLabel.text = state.dataRequest.error

            binding.itemsLayout.isVisible = state.isLoadingSuccess
            binding.emailSalesReport.isVisible = state.isLoadingSuccess

            if (state.isLoadingSuccess) {
                adapter.submitList(state.dataRequest.items)
            }
            if (state.sendReport) {
                mainViewModel.sendAction(MainViewModel.MainViewAction.EmailReportInProgress)
                try {
                    emailLauncher.launch(state.reportEmail.body)
                }catch (e: ActivityNotFoundException) {
                    Toast.makeText(this, R.string.missing_email_app, Toast.LENGTH_LONG).show()
                }
            }

        }

        emailLauncher = (this as ComponentActivity).registerForActivityResult(EmailResultContract()) {
            if (it) {
                mainViewModel.updateInventory()
            }
        }

        binding.emailSalesReport.setOnClickListener {
            SendEmailDialogFragment.newInstance().show(supportFragmentManager, "")
        }
    }

    fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }
}
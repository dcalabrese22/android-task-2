package com.treelineinteractive.recruitmenttask.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.ui.view.ProductItemView

class ProductsRecyclerAdapter : RecyclerView.Adapter<ProductsRecyclerAdapter.ProductViewHolder>(), ItemListener {

    private var data = listOf<ProductItem>()

    private class DiffCallback(
        private val oldList: List<ProductItem>,
        private val newList: List<ProductItem>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }

    fun submitList(data: List<ProductItem>) {
        val old = this.data
        val diffResult = DiffUtil.calculateDiff(DiffCallback(old, data))
        this.data = data
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            ProductItemView(parent.context)
        )
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindData(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun itemClicked(adapterPosition: Int) {
        notifyItemChanged(adapterPosition)
    }

    inner class ProductViewHolder(private val productView: ProductItemView) : RecyclerView.ViewHolder(productView) {

        fun bindData(product: ProductItem) {
            product.position = adapterPosition
            productView.setProductItem(product, this@ProductsRecyclerAdapter)
        }
    }
}

interface ItemListener {
    fun itemClicked(adapterPosition: Int)
}
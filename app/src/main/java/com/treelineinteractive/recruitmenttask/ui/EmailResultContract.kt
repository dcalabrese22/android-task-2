package com.treelineinteractive.recruitmenttask.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.activity.result.contract.ActivityResultContract
import java.time.LocalDate

class EmailResultContract : ActivityResultContract<String, Boolean>() {

    override fun createIntent(context: Context, input: String?): Intent {
        return Intent(Intent.ACTION_SENDTO).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_EMAIL, arrayOf("bossman@bosscompany.com"))
            putExtra(
                Intent.EXTRA_SUBJECT,
                "Sales Report for ${LocalDate.now()}"
            )
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_TEXT, input)
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Boolean {
        return true
    }
}
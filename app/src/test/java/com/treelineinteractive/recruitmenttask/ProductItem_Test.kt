package com.treelineinteractive.recruitmenttask

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import junit.framework.TestCase.*
import org.junit.Test

class ProductItem_Test {

    val product = ProductItem(
        "a", "test", "blue", 1, 2.45f, "Test", ""
    )

    @Test
    fun `test addSale`() {
        assertTrue(product.canSell())
        assertEquals(0, product.sold)
        assertEquals(1, product.available)
        product.addSale()
        assertEquals(1, product.sold)
        assertEquals(0, product.available)
        assertFalse(product.canSell())
    }

    @Test
    fun `test removeSale`() {
        assertFalse(product.canRemoveSale())
        product.addSale()
        assertTrue(product.canRemoveSale())
        product.removeSale()
        assertEquals(1, product.available)
    }
}
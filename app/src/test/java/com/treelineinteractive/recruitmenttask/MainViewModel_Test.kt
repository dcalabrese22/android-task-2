package com.treelineinteractive.recruitmenttask

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.repository.Repository
import com.treelineinteractive.recruitmenttask.data.repository.RepositoryRequestStatus
import com.treelineinteractive.recruitmenttask.data.repository.RepositoryResult
import com.treelineinteractive.recruitmenttask.ui.MainViewModel
import junit.framework.TestCase.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

class MainViewModel_Test {

    val mockShopRepo = MockShopRepo()
    val mainViewModel: MainViewModel by lazy { MainViewModel(mockShopRepo) }
    lateinit var data: List<ProductItem>
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")


    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        val type = object : TypeToken<List<ProductItem>>(){}.type
        data = Gson().fromJson(jsonData, type)

    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `test loadProducts`() {
        runBlocking {
            mainViewModel.loadProducts()
            delay(100)
            assertEquals(data, mainViewModel.stateLiveData.value?.dataRequest?.items)
            assertEquals(false, mainViewModel.stateLiveData.value?.dataRequest?.isLoading)
            assertNull(mainViewModel.stateLiveData.value?.dataRequest?.error)
            assertNull(mainViewModel.stateLiveData.value?.reportEmail?.body)

            mockShopRepo.status = RepositoryRequestStatus.FETCHING
            mainViewModel.loadProducts()
            delay(100)
            assertTrue(mainViewModel.stateLiveData.value?.dataRequest?.isLoading!!)
            assertNull(mainViewModel.stateLiveData.value?.dataRequest?.error)
            assertNull(mainViewModel.stateLiveData.value?.reportEmail?.body)

            mockShopRepo.status = RepositoryRequestStatus.Error(Exception("some error message"))
            mainViewModel.loadProducts()
            delay(100)
            assertFalse(mainViewModel.stateLiveData.value?.dataRequest?.isLoading!!)
            assertEquals("Oops, something went wrong", mainViewModel.stateLiveData.value?.dataRequest?.error)
            assertNull(mainViewModel.stateLiveData.value?.reportEmail?.body)
        }
    }

    @Test
    fun `test createSalesReport`() {
        runBlocking {
            mainViewModel.loadProducts()

            mainViewModel.createSalesReport()
            delay(100)
            assertEquals("Sales Report for ${LocalDate.now()}\n\n${data[0].title}\nSold: ${data[0].sold}\n\n" +
                    "${data[1].title}\n" +
                    "Sold: ${data[1].sold}\n" +
                    "\n" +
                    "${data[2].title}\n" +
                    "Sold: ${data[2].sold}\n" +
                    "\n", mainViewModel.stateLiveData.value?.reportEmail?.body
            )
        }
    }

    @Test
    fun `test updateInventory`() {
        runBlocking {
            mainViewModel.loadProducts()
            delay(100)
            mainViewModel.stateLiveData.value?.dataRequest?.items!!.forEach {
                for (i in 0..(0..10).random()) {
                    it.addSale()
                }
            }
            mainViewModel.stateLiveData.value?.dataRequest?.items!!.forEach {
                assertTrue(it.sold > 0)
            }
            mainViewModel.updateInventory()
            delay(100)
            assertNull(mainViewModel.stateLiveData.value?.reportEmail?.body)
            mainViewModel.stateLiveData.value?.dataRequest?.items!!.forEach {
                assertTrue(it.sold == 0)
            }
        }
    }

    inner class MockShopRepo(var status: RepositoryRequestStatus = RepositoryRequestStatus.COMPLETE) : Repository {
        override fun getProducts(): Flow<RepositoryResult<List<ProductItem>>> {
            return flowOf(RepositoryResult(data, status))
        }
    }
}

val jsonData = "[\n" +
        "  {\n" +
        "    \"id\": \"6b3e607f-c947-40df-9bf0-dfc89ae60e29\",\n" +
        "    \"type\": \"Shirt\",\n" +
        "    \"color\": \"magenta\",\n" +
        "    \"available\": 88,\n" +
        "    \"title\": \"Fresh\",\n" +
        "    \"description\": \"The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients\"\n" +
        "  },\n" +
        "  {\n" +
        "    \"id\": \"7cf77552-0b8e-416e-a613-81dbba64a5aa\",\n" +
        "    \"type\": \"Shirt\",\n" +
        "    \"color\": \"lime\",\n" +
        "    \"available\": 68,\n" +
        "    \"title\": \"Rubber\",\n" +
        "    \"description\": \"The Football Is Good For Training And Recreational Purposes\"\n" +
        "  },\n" +
        "  {\n" +
        "    \"id\": \"61113a59-07a4-41a0-a6a7-ea444d3951c0\",\n" +
        "    \"type\": \"Shirt\",\n" +
        "    \"color\": \"teal\",\n" +
        "    \"available\": 96,\n" +
        "    \"title\": \"Wooden\",\n" +
        "    \"description\": \"The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality\"\n" +
        "  }\n" +
        "]"